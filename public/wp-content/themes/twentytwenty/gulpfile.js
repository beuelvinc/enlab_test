var gulp = require('gulp');
var rename = require('gulp-rename');

var filever = require('gulp-version-filename');

gulp.task('rename', function() {
  return gulp.src(['*.css', '*.js'])
    .filever({
      key: 'package', // Searches for @package
      silent: true // Suppress errors if file is missing @package
    })
    .pipe(gulp.dest('dist'));
});
